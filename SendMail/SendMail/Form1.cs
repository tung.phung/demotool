﻿using Aspose.Words;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Web;
using iTextSharp.text.pdf.parser;

namespace SendMail
{
    public partial class Form1 : Form
    {
        private DataTable dt = new DataTable();
        private SqlConnection con = new SqlConnection();
        private DataSet ds = new DataSet();
        string desktopPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
        public static string valuePdf = "";

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            con = ConnectionDB.myConnection();

            con.Open();
            string sql = "SELECT u.id, u.name, u.mail, l.luong, l.phat, l.bonus FROM Tbl_User u INNER JOIN Tbl_luong l ON u.id = l.id";
            SqlCommand cmd = new SqlCommand(sql, con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds, "Data");
            con.Close();
            DataGridViewButtonColumn btnShow = new DataGridViewButtonColumn();
            btnShow.Name = "btnShow";
            btnShow.HeaderText = "View";
            //dtgList.CellClick += btnShow_Click;
            dtgList.Columns.Insert(1, btnShow);
            dtgList.DataSource = ds;
            dtgList.DataMember = "Data";
            dt = ds.Tables["Data"];
        }

        private void ExtractWord(DataRow dr, string file)
        {

            string filePath = desktopPath + @"\phieuluong.doc";
            Document document = new Document(filePath);
            try
            {
                DocumentBuilder db = new DocumentBuilder(document);
                db.Writeln("Phiếu lương tháng " + DateTime.Now.Month + "/" + DateTime.Now.Year);
                document.MailMerge.Execute(dr);
                document.Save(desktopPath + @"\" + file + ".pdf");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void btnSend_Click(object sender, EventArgs e)
        {

            foreach (DataGridViewRow item in dtgList.Rows)
            {
                if (Convert.ToBoolean(item.Cells["cbCheck"].Value))
                {
                    int i = item.Index;

                    this.ExtractWord(dt.Rows[i], dt.Rows[i]["mail"].ToString());

                    this.Send_Mail(dt.Rows[i]["mail"].ToString());

                }
            }


        }
        private void btnShow_Click(object sender, DataGridViewCellEventArgs e)
        {
            //string file = desktopPath + @"\" + dt.Rows[e.ColumnIndex]["mail"].ToString() + ".pdf";

            //if (e.RowIndex == dtgList.Rows["btnShow"].Index)
            //{
            //    try
            //    {
            //        iTextSharp.text.pdf.PdfReader reader = new iTextSharp.text.pdf.PdfReader(file);
            //        StringBuilder sb = new StringBuilder();
            //        for (int i = 1; i <= reader.NumberOfPages; i++)
            //        {
            //            //Read page
            //            sb.Append(PdfTextExtractor.GetTextFromPage(reader, i));
            //        }
            //        PreviewPDF form = new PreviewPDF();
            //        valuePdf = sb.ToString();

            //        form.Show();
            //        reader.Close();
            //    }
            //    catch (Exception ex)
            //    {
            //        MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //    }
            //}
        }

        private void Send_Mail(string toMail)
        {
            string desktopPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            MailMessage mail = new MailMessage();
            SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");
            mail.From = new MailAddress("tungpt1@vmodev.com");
            mail.To.Add(toMail);
            mail.Subject = "Test Mail - 1";
            mail.Body = "mail with attachment";
            string file = toMail;

            System.Net.Mail.Attachment attachment;
            attachment = new System.Net.Mail.Attachment(desktopPath + @"\" + file + ".pdf");
            mail.Attachments.Add(attachment);

            SmtpServer.Port = 587;
            SmtpServer.UseDefaultCredentials = false;

            SmtpServer.Credentials = new System.Net.NetworkCredential("tungpt1@vmodev.com", "rtyfghvbn5@");
            SmtpServer.EnableSsl = true;

            SmtpServer.Send(mail);
        }

        private void dtgList_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var senderGird = (DataGridView)sender;

            if (senderGird.Columns[e.ColumnIndex] is DataGridViewButtonColumn && e.RowIndex >= 0)
            {
                string file = desktopPath + @"\" + dt.Rows[e.RowIndex]["mail"].ToString() + ".pdf";


                try
                {
                    iTextSharp.text.pdf.PdfReader reader = new iTextSharp.text.pdf.PdfReader(file);
                    StringBuilder sb = new StringBuilder();
                    for (int i = 1; i <= reader.NumberOfPages; i++)
                    {
                        //Read page
                        sb.Append(PdfTextExtractor.GetTextFromPage(reader, i));
                    }
                    PreviewPDF form = new PreviewPDF();
                    valuePdf = sb.ToString();

                    form.Show();
                    reader.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
    }
}
