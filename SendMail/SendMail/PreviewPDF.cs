﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace SendMail
{
    public partial class PreviewPDF : Form
    {
        public PreviewPDF()
        {
            InitializeComponent();
        }

        private void PreviewPDF_Load(object sender, EventArgs e)
        {
            richTextBox.Text = Form1.valuePdf;
        }
    }
}
